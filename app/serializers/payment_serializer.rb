class PaymentSerializer
  include FastJsonapi::ObjectSerializer
  attributes :account_number
  belongs_to :user
  belongs_to :business
end
