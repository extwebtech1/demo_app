class UserSerializer
  include FastJsonapi::ObjectSerializer
  attributes *[
   :first_name, 
   :email, 
   :mobile,
   :password,
   :password_confirmation
 ]
   

   attribute :business do |object|
      object.businesses
    end

    attribute :payment do |object|
      object.payment
    end

   has_many :businesses
   has_one :payment
end
