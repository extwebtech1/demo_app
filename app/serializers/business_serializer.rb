class BusinessSerializer
  include FastJsonapi::ObjectSerializer
  attributes :gst_number
  belongs_to :user
  has_one :payment
end
