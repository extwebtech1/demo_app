class Payment < ApplicationRecord
  validates :account_number, uniqueness: true
  validates :bank_name, presence: true
  validates :ifsc_code, presence: true
  belongs_to :user
  belongs_to :business
end
