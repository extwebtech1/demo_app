class User < ApplicationRecord
  has_secure_password
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :password, length: { minimum: 6 }
  validates :email, uniqueness: true
  has_many :businesses, dependent: :destroy
  has_one  :payment, dependent: :destroy
end
