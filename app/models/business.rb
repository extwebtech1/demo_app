class Business < ApplicationRecord
  validates :business_name, presence: true
  belongs_to :user
  has_one :payment, dependent: :destroy
end
