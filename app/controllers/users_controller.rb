class UsersController < ApplicationController
  before_action :set_user, only: %i[show update destroy]
  before_action :check_owner, only: %i[update destroy]



  def index
    @users = User.all
    render json: UserSerializer.new(@users).serializable_hash
  end 

  def show
    @user = User.find(params[:id])
    if @user
      options = { include: [:businesses] }
      render json: UserSerializer.new(@user, options).serializable_hash
     else
      head 404
    end
  end

  def login

    @user = User.find_by_email(params[:email]) 
    if @user&.authenticate(params[:password])
      render json: {
      token: JsonWebToken.encode(user_id: @user.id),
      data: @user
      }
      else
      render json:{
      message: "email or password is incorrect"
      }
    end
  end


  def create
    @user = User.create(user_params)
    if @user.save
      render json: @user, status: :created
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  def update
    if @user.update(user_params)
      render json: @user
    else
      render json: @user.errors
    end
  end

  def destroy
    @user.destroy
    head 204
  end

  def businesses
      render json: User::BusinessSerializer.new(User::Business.all)
    end

    def payments
      render json: User::PaymentSerializer.new(User::Payment.all)
    end

  private
    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :mobile, :password, :password_confirmation)
    end
end
