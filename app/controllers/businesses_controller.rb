class BusinessesController < ApplicationController
  before_action :set_business, only: [:show, :update, :destroy]
 

  def index
    @businesses = Business.all
    render json: BusinessSerializer.new(@businesses).serializable_hash
  end

  def show 
    @business = Business.find(params[:id])
    if @business
    options = {include: [:user]}
    render json: BusinessSerializer.new(@business , options).serializable_hash
     else
      head 404
    end
  end  

  def create

    @business = Business.new(business_params)
    if @business.save
      render json: @business
    else
      render json: @business.errors
    end
  end
  def update
    if @business.update(business_params)
      render json: @business
    else
      render json: @business.errors
    end
  end

  def destroy 
    @business.destroy 
    head :no_content
  end

  private

  def set_business 
    @business = Business.find(params[:id])
  end

  def business_params 
    params.require(:business).permit(:business_name, :gst_number, :business_add, :user_id)
  end
end
