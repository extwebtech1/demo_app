class PaymentsController < ApplicationController
  before_action :set_payment, only: %i[show update destroy]

  def index
    @payments = Payment.all
    render json: PaymentSerializer.new(@payments).serializable_hash
  end


  def show 
    @payment =Payment.find(params[:id])
    if @payment
      options = { include: [:payment]}
      render json: PaymentSerializer.new(@payment).serializable_hash
      else
       head 404
    end
  end

  def create
    @payment = Payment.new(payment_params)
    if @payment.save
      render json: @payment, status: :created
    else
      render json: @payment.errors, status: :unprocessable_entity
    end      
  end

  private
  def set_payment
    @payment = Payment.find(params[:id])
  end

  def payment_params
    params.require(:payment).permit(:account_number, :bank_name, :ifsc_code, :user_id, :business_id)
  end
end
