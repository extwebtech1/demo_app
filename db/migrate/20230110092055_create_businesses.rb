class CreateBusinesses < ActiveRecord::Migration[6.1]
  def change
    create_table :businesses do |t|
      t.string :business_name
      t.integer :gst_number
      t.string :business_add
      t.references :user, null: false, foreign_key: true


      t.timestamps
    end
  end
end
