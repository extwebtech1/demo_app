require 'rails_helper'

RSpec.describe "Payments", type: :request do
  # describe "GET /payments" do
  #   it "works! (now write some real specs)" do
  #     get payments_index_path
  #     expect(response).to have_http_status(200)
  #   end
  # end

  describe "POST Create Payment" do
    it "create payment" do
      post "/payments", params:{payment:{
                                                                
           account_number:"1234567890", bank_name:"hdfc", ifsc_code:"HDFC123", user_id: "1", business_id: "1"
    }
      } 
                                             
      expect(JSON.parse(response.body)['account_number']).to eq("1234567890")
      expect(response).to have_http_status(201)
    end
  end
end
