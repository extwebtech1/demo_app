require 'rails_helper'

RSpec.describe "Businesses", type: :request do
  before(:each) do |example|
  # describe "GET /businesses" do
  #   it "works! (now write some real specs)" do
  #     get businesses_index_path
  #     expect(response).to have_http_status(200)
  #   end
   end

  describe "POST Create Business" do
    it "create business" do
      post "/businesses", params:{business:{
                                                                
           business_name:"sonu", gst_number:"65463", business_add:"rewa", user_id: "1"
    }
      } 
                                             
      expect(JSON.parse(response.body)['business_name']).to eq("sonu")
      expect(response).to have_http_status(200)
    end
  end

  describe "GET particular business" do
    it "get particular business" do
      get "/businesses/#{@business_id}"
      data = JSON.parse(response.body)
      expect(response).to have_http_status(200)
    end
  end
end
