require 'rails_helper'

RSpec.describe Business, type: :model do
  context "validation business" do
    it "ensure presence of some attributes" do
      business = Business.create()
      expect(business.errors.messages[:business_name]).to eq(["can't be blank"])      
    end
  end

  context "association test" do
    it "should belongs_to user " do
      t = Business.reflect_on_association(:user)
      expect(t.macro).to eq(:belongs_to)
    end
  end
end
