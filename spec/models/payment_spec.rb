require 'rails_helper'

RSpec.describe Payment, type: :model do
 context "validation payment" do
    it "ensure presence of some attributes" do
      payment = Payment.create()
      expect(payment.errors.messages[:bank_name]).to eq(["can't be blank"])
      expect(payment.errors.messages[:ifsc_code]).to eq(["can't be blank"])      
    end
  end
end
