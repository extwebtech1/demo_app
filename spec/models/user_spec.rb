require 'rails_helper'

RSpec.describe User, type: :model do

  context "association test" do
    it "should has_many businesses " do
      t = User.reflect_on_association(:businesses)
      expect(t.macro).to eq(:has_many)
    end
  end


 context "validation user" do
    it "ensure presence of some attributes" do
      user = User.create()
      expect(user.errors.messages[:first_name]).to eq(["can't be blank"])
      expect(user.errors.messages[:last_name]).to eq(["can't be blank"])
    end
  end
end
