Rails.application.routes.draw do
  get 'users', to: 'users#index'

  post 'users', to: 'users#create'

  get 'users/:id', to: 'users#show'

  put 'users/:id', to: 'users#update'

  delete 'users/:id', to: 'users#delete'
  post 'login', to: 'users#login'

  resources :businesses
  resources :payments
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
